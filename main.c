#include <stdio.h>
#include <stdlib.h>

// robert comment
#define SAMPLE_SIZE 13

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int
GetNext() {
  static int i = 0;
  static int sample_input[SAMPLE_SIZE] =
	{ 0xf12, 0xea2, 0x684, 0xfff0, 0xfff, 0x1f5, 0xf12, 0xea2, 0x684, 0xfff,
    0x1f5,0x552,0x550
  };

  if (i < SAMPLE_SIZE)
      return sample_input[i++];
  else
      return -1;
}


int cmp(const void *a, const void *b){
	return (*(int*)b - *(int*)a);
}

void ShowOnceData(void){
	int tableVar[SAMPLE_SIZE]; //tab containing recv data
	int getVal;
	
	for (int i=0; i<SAMPLE_SIZE;i++){
		tableVar[i]=GetNext();
	} //get elements
	
	//sort
	qsort(tableVar, SAMPLE_SIZE,sizeof(int), cmp);
	
	if (tableVar[0]!=tableVar[1]){ //start test
		printf("%x ONCE!\n",tableVar[0]);		
	}
	
	for (int i=1; i<SAMPLE_SIZE-1;i++){ // middle tests
		
		if ((tableVar[i]!=tableVar[i+1])&&(tableVar[i]!=tableVar[i-1])){
			printf("%x ONCE!\n",tableVar[i]);	
		}
	}
	
	if (tableVar[SAMPLE_SIZE-2]!=tableVar[SAMPLE_SIZE-1]){ //end test
		printf("%x ONCE!\n",tableVar[SAMPLE_SIZE-1]);		
	}
}

int main(int argc, char *argv[]) {
		
	ShowOnceData();
	
}
